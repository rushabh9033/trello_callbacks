const boardData = require("../data/boards.json");
const listsData = require("../data/lists.json");
const cardsData = require("../data/cards.json");

const thanosBoardInfo = require("../callback4");
const boardInform = require("../callback1");
const boardsInListData = require("../callback2");
const cardsInListData = require("../callback3");

thanosBoardInfo(
    boardInform,
    boardsInListData,
    cardsInListData,
    boardData,
    listsData,
    cardsData,
    "mcu453ed"
);
