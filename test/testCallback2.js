const listData = require("../data/lists.json");
const boardsInListData = require("../callback2");
const callbackFn = (data, err) => {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
};
boardsInListData(listData, "mcu453ed", callbackFn);
