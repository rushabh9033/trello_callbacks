const boardData = require("../data/boards.json");
const boardInform = require("../callback1");
const callbackFn = (data, err) => {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
};
boardInform(boardData, "mcu453ed", callbackFn);
