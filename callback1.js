/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID 
    that is passed from the given list of boards in boards.json and then pass control back to the code 
    that called it by using a callback function.  
*/
function boardInform(boardData, Id, callBack) {
  let findSpecificData = boardData.find((element) => element.id == Id);
  setTimeout(() => {
    if (!findSpecificData) {
      callBack("There is no id", null);
    } else {
      callBack(null, findSpecificData);
    }
  }, 2000);
}
module.exports = boardInform;
