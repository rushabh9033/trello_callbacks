/* 
  Problem 4: Write a function that will use the previously written functions to get the following information. 
    You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

function thanosBoardInfo(boardInform, boardsInListData, cardsInListData, boardData, listData, cardData, thanosId) {
    setTimeout(() => {
        boardInform(boardData, thanosId, (err, data) => {
            if (err) {
                console.log(err);
            } else {
                console.log(data);
                boardsInListData(listData, thanosId, (err, data) => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(data);
                        let mindId = data.find((element) => element.name == "Mind").id;
                        cardsInListData(cardData, mindId, (err, data) => {
                            if (err) {
                                console.log(err)
                            } else {
                                console.log(data)
                            }
                        })
                    }
                });
            }
        }, 2000);
    });
}
module.exports = thanosBoardInfo;
