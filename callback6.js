/*
    Problem 6: Write a function that will use the previously written functions to get the following information.
    You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

function thanosBoardAllInfo(boardInform, boardsInListData, cardsInListData, boardData, listData, cardData) {
    setTimeout(() => {
        let thanosId = boardData.find((element) => element.name == "Thanos").id
        boardInform(boardData, thanosId, (data, err) => {
            if (err) {
                console.log(err)
            } else {
                console.log(data)
            }
            boardsInListData(listData, thanosId, (err, data) => {
                if (err) {
                    console.log(err)
                } else {
                    console.log(data)
                }
                data.map(element => {
                    if (cardData[element.id]) {
                        cardsInListData(cardData, element.id, (err, data) => {
                            if (err) {
                                console.log(err)
                            } else {
                                console.log(data)
                            }
                        })
                    }
                });
            })
        })
    }, 2000)
}
module.exports = thanosBoardAllInfo;