/* 
	Problem 2: Write a function that will return all lists that belong to a board based on 
    the boardID that is passed to it from the given data in lists.json. 
    Then pass control back to the code that called it by using a callback function.
*/

function boardsInListData(listData, Id, callBack) {
  setTimeout(() => {
    if (!listData.hasOwnProperty(Id)) {
      callBack("There is no id.", null);
    } else {
      callBack(null, listData[Id]);
    }
  }, 2000);
}
module.exports = boardsInListData;
