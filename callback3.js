/* 
  Problem 3: Write a function that will return all cards that belong to a particular list based 
    on the listID that is passed to it from the given data in cards.json. 
    Then pass control back to the code that called it by using a callback function.
*/

function cardsInListData(cardData, Id, callBack) {
  setTimeout(() => {
    if (!cardData[Id]) {
      callBack("There is no id.", null);
    } else {
      callBack(null, cardData[Id]);
    }
  }, 2000);
}
module.exports = cardsInListData;
