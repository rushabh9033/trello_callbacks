/* 
    Problem 5: Write a function that will use the previously written functions to get the following information.
     You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/


function thanosBoardInfo2(boardInform, boardsInListData, cardsInListData, boardData, listData, cardData, thanosId) {
    setTimeout(() => {
        boardInform(boardData, thanosId, (err, data) => {
            if (err) {
                console.log(err);
            } else {
                console.log(data);

                boardsInListData(listData, thanosId, (err, data) => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(data);

                        let mindId = data.find((element) => element.name == "Mind").id;
                        let spaceId = data.find((element) => element.name == "Space").id
                        cardsInListData(cardData, mindId, (err, data) => {
                            if (err) {
                                console.log(err)
                            } else {
                                console.log(data)
                            }
                        })
                        cardsInListData(cardData, spaceId, (err, data) => {
                            if (err) {
                                console.log(err)
                            } else {
                                console.log(data)
                            }
                        })

                    }
                });
            }
        }, 2000);
    });
}
module.exports = thanosBoardInfo2;
